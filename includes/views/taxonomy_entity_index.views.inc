<?php

/**
 * Implements hook_views_data().
 */
function taxonomy_entity_index_views_data() {
  $entity_info = entity_get_info();
  $data = array();

  // table {taxonomy_entity_index}
  $data['taxonomy_entity_index']['table']['group']  = t('Taxonomy Entity Index');
  $data['taxonomy_entity_index']['table']['join'] = array(
    'taxonomy_term_data' => array(
      // links directly to taxonomy_term_data via tid
      'left_field' => 'tid',
      'field' => 'tid',
    ),
    'taxonomy_term_hierarchy' => array(
      'left_field' => 'tid',
      'field' => 'tid',
    ),
  );

  $data['taxonomy_entity_index']['tid'] = array(
    'group' => t('Taxonomy Entity Index'),
    'title' => t('Has taxonomy term ID'),
    'help' => t('Display entities if it has the selected taxonomy terms.'),
    'argument' => array(
      'handler' => 'taxonomy_entity_index_handler_argument_taxonomy_entity_index_tid',
      'name table' => 'taxonomy_term_data',
      'name field' => 'name',
      'empty field name' => t('Uncategorized'),
      'numeric' => TRUE,
      'skip base' => 'taxonomy_term_data',
    ),
    'filter' => array(
      'title' => t('Has taxonomy term'),
      'handler' => 'taxonomy_entity_index_handler_filter_taxonomy_entity_index_tid',
      'hierarchy table' => 'taxonomy_term_hierarchy',
      'numeric' => TRUE,
      'skip base' => 'taxonomy_term_data',
      'allow empty' => TRUE,
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function taxonomy_entity_index_views_data_alter(&$data) {
  $entity_info = taxonomy_entity_index_entity_views_integrable($data);
  foreach($entity_info as $type => $info) {
    if (isset($data[$info['base table']]) && isset($info['entity keys']['id'])) {
      $base_table = $info['base table'];
      $data[$base_table]['taxonomy_entity_index_tid_depth'] = array(
        'help' => t('Display content if it has the selected taxonomy terms, or children of the selected terms. Due to additional complexity, this has fewer options than the versions without depth.'),
        'real field' => isset($info['entity keys']['revision']) ? $info['entity keys']['revision'] : $info['entity keys']['id'],
        'argument' => array(
          'title' => t('Has taxonomy term ID (with depth and indexed in taxonomy_entity_index)'),
          'handler' => 'taxonomy_entity_index_handler_argument_tid_depth',
          'accept depth modifier' => TRUE,
          'entity type' => $type,
        ),
        'filter' => array(
          'title' => t('Has taxonomy terms (with depth and indexed in taxonomy_entity_index)'),
          'handler' => 'taxonomy_entity_index_handler_filter_taxonomy_entity_index_tid_depth',
          'entity type' => $type,
        ),
      );

      $data[$base_table]['taxonomy_entity_index_entity_tid'] = array(
        'group' => t('Taxonomy Entity Index'),
        'title' => t('Taxonomy terms on @entity_type', array('@entity_type' => $info['label'])),
        'help' => t('Relate @entity_type to taxonomy terms. This relationship will cause duplicated records if there are multiple terms.', array('@entity_type' => $info['label'])),
        'relationship' => array(
          'real field' => $info['entity keys']['id'],
          'label' => t('terms'),
          'base' => 'taxonomy_entity_index',
          'base field' => 'entity_id',
          'extra' => array(
            array(
              'field' => 'entity_type',
              'operator' => '=',
              'value' => $type,
            ),
          ),
        ),
        'field' => array(
          'title' => t('All taxonomy terms on @entity_type', array('@entity_type' => $info['label'])),
          'help' => t('Display all taxonomy terms associated with a @entity_type from specified vocabularies.', array('@entity_type' => $info['label'])),
          'handler' => 'taxonomy_entity_index_handler_field_taxonomy_entity_index_tid',
          'no group by' => TRUE,
        ),
      );
    }
  }

  $data['taxonomy_term_data']['table']['join'] = array(
    'taxonomy_entity_index' => array(
      // links directly to taxonomy_term_data via tid
      'left_field' => 'tid',
      'field' => 'tid',
    ),
  );
}
